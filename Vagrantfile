#####################################GLOBAL SETTINGS#################################### 
box="ubuntu/focal64"                                                                   #
network_adapter="enp2s0"                                                               #
netmask="24"                                                                           #
external_port="8080"                                                                   #
#########################################VMS IP######################################### 
master_ip="192.168.88.11"                                                              #
slave_ip="192.168.88.12"                                                               #
wordpress_ip="192.168.88.13"                                                           #
#################################REPLICATION CREDENTIAL################################# 
REPL_USER="repl_user"                                                                  #
REPL_PASS="123"                                                                        #
###################################DATABASE CREDENTIAL################################## 
DB_NAME="wordpress"                                                                    #
DB_USER="wp_user"                                                                      #
DB_PASS="123"                                                                          #
DB_HOST="192.168.88.11"                                                                # 
########################################################################################

Vagrant.configure("2") do |config|
  config.vm.box = "#{box}"
  config.vm.define "master" do |master|
    master.vm.network "public_network",bridge: "#{network_adapter}",
        ip: "#{master_ip}",
        netmask: "#{netmask}" 
    master.vm.provision "shell", inline: <<-SHELL  
      sudo apt-get update -y
      sudo apt-get install postgresql -y
      
      sudo -u postgres psql -c "CREATE ROLE #{REPL_USER} WITH REPLICATION LOGIN PASSWORD '#{REPL_PASS}';"
      
      sudo echo "listen_addresses = '#{master_ip}'" >> /etc/postgresql/12/main/postgresql.conf
      sudo echo "wal_level = logical" >> /etc/postgresql/12/main/postgresql.conf
      sudo echo "host    replication     #{REPL_USER}    #{slave_ip}/32   trust" >> /etc/postgresql/12/main/pg_hba.conf
      sudo echo "host    all     #{DB_USER}    #{wordpress_ip}/32   trust" >> /etc/postgresql/12/main/pg_hba.conf
      
      sudo systemctl restart postgresql
      
      sudo -u postgres psql -c "CREATE DATABASE #{DB_NAME};"
      sudo -u postgres psql -c "CREATE USER #{DB_USER} WITH PASSWORD '#{DB_PASS}';"
      sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE #{DB_NAME} TO #{DB_USER};"
    SHELL
  end

  config.vm.define "slave" do |slave|
    slave.vm.network "public_network",bridge: "#{network_adapter}",
        ip: "#{slave_ip}",
        netmask: "#{netmask}"
    slave.vm.provision "shell", inline: <<-SHELL  
      sudo apt-get update -y
      sudo apt-get install postgresql -y

      sudo systemctl stop postgresql
      sudo -u postgres rm -r /var/lib/postgresql/12/main/*
      sudo pg_basebackup -h #{master_ip} -U #{REPL_USER} -X stream -C -S replica_1 -v -R -w -D /var/lib/postgresql/12/main/
      sudo chown -R postgres /var/lib/postgresql/12/main/
      sudo systemctl start postgresql
    SHELL
  end
  
  config.vm.define "wordpress" do |wordpress|
    wordpress.vm.network "public_network",bridge: "#{network_adapter}",
        ip: "#{wordpress_ip}",
        netmask: "#{netmask}"
    wordpress.vm.network "forwarded_port", guest: 80, host: "#{external_port}", host_ip: "127.0.0.1"
    wordpress.vm.provision "shell", inline: <<-SHELL   
      sudo apt-get update -y
      sudo apt-get install apache2 -y
      sudo apt-get install php php-pgsql php-mysql libapache2-mod-php -y
      sudo apt-get install git -y

      sudo wget https://wordpress.org/latest.tar.gz
      sudo tar -xvzf latest.tar.gz
      rm /var/www/html/index.html
      sudo mv wordpress/* /var/www/html/
      sudo rm latest.tar.gz
      sudo rm -r wordpress
      
      git clone https://github.com/kevinoid/postgresql-for-wordpress.git
      sudo mv postgresql-for-wordpress/pg4wp /var/www/html/wp-content/pg4wp
      rm -rf postgresql-for-wordpress
      sudo cp /var/www/html/wp-content/pg4wp/db.php /var/www/html/wp-content/
      sudo cp /var/www/html/wp-config-sample.php /var/www/html/wp-config.php
      
      sudo sed -i 's/database_name_here/#{DB_NAME}/' /var/www/html/wp-config.php
      sudo sed -i 's/username_here/#{DB_USER}/'  /var/www/html/wp-config.php
      sudo sed -i 's/password_here/#{DB_PASS}/'  /var/www/html/wp-config.php
      sudo sed -i 's/localhost/#{master_ip}/'  /var/www/html/wp-config.php
      
      sudo chown -R www-data:www-data /var/www/html/
    SHELL
  end

end
